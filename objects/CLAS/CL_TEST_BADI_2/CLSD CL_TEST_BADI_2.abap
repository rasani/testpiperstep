class-pool .
*"* class pool for class CL_TEST_BADI_2

*"* local type definitions
include CL_TEST_BADI_2================ccdef.

*"* class CL_TEST_BADI_2 definition
*"* public declarations
  include CL_TEST_BADI_2================cu.
*"* protected declarations
  include CL_TEST_BADI_2================co.
*"* private declarations
  include CL_TEST_BADI_2================ci.
endclass. "CL_TEST_BADI_2 definition

*"* macro definitions
include CL_TEST_BADI_2================ccmac.
*"* local class implementation
include CL_TEST_BADI_2================ccimp.

*"* test class
include CL_TEST_BADI_2================ccau.

class CL_TEST_BADI_2 implementation.
*"* method's implementations
  include methods.
endclass. "CL_TEST_BADI_2 implementation
