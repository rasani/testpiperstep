*"* use this source file for your ABAP unit test classes
class ltcl_test_badi_2 definition for testing
                                               duration short
                                               risk level harmless
                                               final.

private section.

 methods:
      first_test_method for testing,
      second_test_method for testing.


endclass.

class ltcl_test_badi_2 implementation.






  method first_test_method.


  data(test) = 'Hello World'.

  cl_abap_unit_assert=>assert_equals(  act = test exp = 'Hello World' level = '0' ).





  endmethod.

  method second_test_method.

  data(test) = 'Amigo'.

  cl_abap_unit_assert=>assert_equals(  act = test exp = 'Amigo' ).
    cl_test_class_new=>first_method(  ).


  endmethod.

endclass.