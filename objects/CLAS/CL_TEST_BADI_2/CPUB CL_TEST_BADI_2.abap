class CL_TEST_BADI_2 definition
  public
  final
  create public .

public section.

*interfaces IF_TEST_BADI_2.

  types:
*   definition here
    begin of ty_get_file_content_request,
        repository  type string,
        prefix      type string,
        file_name   type string,
        object_type type string,
        object_name type string,
        prefix_test type string,
      end of ty_get_file_content_request .

  class-methods TEST_METHOD1 .
  class-methods TEST_METHOD2 .
  class-methods TEST_METHOD3 .
  class-methods TEST_METHOD5 .